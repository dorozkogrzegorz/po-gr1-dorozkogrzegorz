package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z1f {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Wpisz rozmiar tablicy: ");
        int n = input.nextInt();
        int tab[] = new int[n];
        for(int i=0; i<n; i++){
            tab[i]=generator.nextInt()%1000;
            if(tab[i]>0) tab[i]=1;
            else tab[i]=-1;
        }
        System.out.println("Zmodyfikowana tablica:");
        for(int i=0; i<n; i++){
            System.out.print(tab[i] + " ");
        }
    }
}

package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z2g {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Wpisz rozmiar tablicy w przedziale <0, 100>: ");
        int n = input.nextInt();
        System.out.println("Podaj \"lewy\" element: ");
        int lewy = input.nextInt();
        System.out.println("Podaj \"prawy\" element: ");
        int prawy = input.nextInt();
        if (n >= 1 && n <= 100 && lewy >= 1 && prawy > lewy && prawy <= n) {
            int tab[] = new int[n];
            generuj(tab, n, -999, 999);
            wyswietl(tab);
            odwrocFragment(tab, lewy, prawy);
            wyswietl(tab);
        }
        else
            System.out.println("Blad. Liczba spoza przedzialu lub wartosc ktoregos z elementow wyzsza od rozmiaru tablicy.");
    }
    public static void generuj(int tab[], int n, int min, int max) {
        Random generator = new Random();
        int x;
        if (min >= 0) {
            x = max - min + 1;
        }
        else x = (-1)*min + max + 1;
        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(x) + min;
        }
    }
    public static void wyswietl(int tab[]){
        System.out.println("Wygenerowane elementy tablicy: ");
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println("\n");
    }
    public static void odwrocFragment (int tab[] , int lewy, int prawy){
        int a, x=0;
        for(int i=lewy-1; i<prawy-1; i++){
            a=tab[i];
            tab[i]=tab[prawy-1-x];
            tab[prawy-1-x]=a;
            if(i==(prawy-1-x))
                break;
            x++;
        }
    }
}

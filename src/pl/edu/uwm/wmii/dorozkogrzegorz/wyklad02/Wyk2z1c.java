package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z1c {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Wpisz rozmiar tablicy: ");
        int n = input.nextInt();
        int tab[] = new int[n];
        int max=-1000, a=0;
        for(int i=0; i<n; i++){
            tab[i]=generator.nextInt()%1000;
            if(tab[i]>max){
                max=tab[i];
                a=0;
            }
            if(max==tab[i]){
                a++;
            }
        }
        System.out.println("Najwiekszy element " + max + " wystepuje " + a + " razy.");
    }
}

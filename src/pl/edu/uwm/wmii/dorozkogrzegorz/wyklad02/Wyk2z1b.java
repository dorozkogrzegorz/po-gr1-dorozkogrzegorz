package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z1b {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Wpisz rozmiar tablicy: ");
        int n = input.nextInt();
        int tab[] = new int[n];
        int d=0, u=0, z=0;
        for(int i=0; i<n; i++){
            tab[i]=generator.nextInt()%1000;
            if(tab[i]>0)d++;
            else if(tab[i]<0) u++;
            else z++;
        }
        System.out.println("Elementy dodatnie: " + d + "\nElementy ujemne: " + u + "\nElementy zerowe: " + z);
    }
}

package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z1e {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Wpisz rozmiar tablicy: ");
        int n = input.nextInt();
        int tab[] = new int[n];
        int a=0, max=0;
        for(int i=0; i<n; i++){
            tab[i]=generator.nextInt()%1000;
            if(tab[i]>=0)a++;
            else a=0;
            if(a>max) max=a;
        }
        System.out.println("Dlugosc najdluzszego fragmentu tablicy z dodatnimi elementami: " + max);
    }
}

package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z1d {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Wpisz rozmiar tablicy: ");
        int n = input.nextInt();
        int tab[] = new int[n];
        int sumad=0, sumau=0;
        for(int i=0; i<n; i++){
            tab[i]=generator.nextInt()%1000;
            if(tab[i]>0) sumad=sumad+tab[i];
            else sumau=sumau+tab[i];
        }
        System.out.println("Suma dodatnich elementow: " + sumad + "\nSuma ujemnych elementow: " + sumau);
    }
}

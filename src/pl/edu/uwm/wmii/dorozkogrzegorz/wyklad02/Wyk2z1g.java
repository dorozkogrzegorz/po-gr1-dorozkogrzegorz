package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z1g {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Wpisz rozmiar tablicy: ");
        int n = input.nextInt();
        int tab[] = new int[n];
        int lewy, prawy;
        System.out.println("Podaj \"lewy\" element: ");
        lewy = input.nextInt();
        System.out.println("Podaj \"prawy\" element: ");
        prawy = input.nextInt();
        for(int i=0; i<n; i++){
            tab[i]=generator.nextInt()%1000;
        }
        for(int i=0; i<n; i++){
            System.out.print(tab[i] + " ");
        }
        int a, x=0;
        for(int i=lewy-1; i<prawy-1; i++){
            a=tab[i];
            tab[i]=tab[prawy-1-x];
            tab[prawy-1-x]=a;
            if(i==(prawy-1-x))
                break;
            x++;
        }
        System.out.print("\n");
        for(int i=0; i<n; i++){
            System.out.print(tab[i] + " ");
        }
    }
}

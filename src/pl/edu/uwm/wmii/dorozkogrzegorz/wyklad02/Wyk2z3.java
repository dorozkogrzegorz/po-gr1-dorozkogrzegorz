package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m, n, k;
        System.out.println("Wpisz wartosci dla ponizszych zmiennych z przedzialu <0,10>.");
        System.out.println("Wpisz wartosc m: ");
        m = input.nextInt();
        System.out.println("Wpisz wartosc n: ");
        n = input.nextInt();
        System.out.println("Wpisz wartosc k: ");
        k = input.nextInt();
        int[][] A = new int[m][n];
        int[][] B = new int[n][k];
        generujMacierz(A, m, n, 0, 9);
        generujMacierz(B, n, k, 0, 9);
        System.out.println("Macierz A:");
        wyswietlMacierz(A, m, n);
        System.out.println("Macierz B:");
        wyswietlMacierz(B, n, k);
        iloczynMacierzy(A, B, m, n, k);
    }
    public static void iloczynMacierzy(int A[][], int B[][], int m, int n, int k){
        int[][] C = new int[m][k];
        int a = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                for (int l = 0; l < n; l++) {
                    a = a + (A[i][l] * B[l][j]);
                }
                C[i][j] = a;
                a = 0;
            }
        }
        System.out.println("Wynik mnozenia macierzy (macierz C): ");
        wyswietlMacierz(C, m, k);
    }
    public static void generujMacierz(int tab[][], int a, int b, int min, int max) {
        Random generator = new Random();
        int x;
        if (min >= 0) {
            x = max - min + 1;
        }
        else
            x = (-1)*min + max + 1;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                tab[i][j] = generator.nextInt(x) + min;
            }
        }
    }
    public static void wyswietlMacierz(int tab[][], int a, int b){
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                System.out.print(tab[i][j] + "   ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
    }
}

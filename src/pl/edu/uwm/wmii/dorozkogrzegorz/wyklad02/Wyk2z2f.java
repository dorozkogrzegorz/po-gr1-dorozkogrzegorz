package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad02;
import java.util.Scanner;
import java.util.Random;

public class Wyk2z2f {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Wpisz rozmiar tablicy w przedziale <0, 100>: ");
        int n = input.nextInt();
        if (n >= 1 && n <= 100) {
            int tab[] = new int[n];
            generuj(tab, n, -999, 999);
            wyswietl(tab);
            signum(tab);
            wyswietl(tab);
        }
        else
            System.out.println("Blad. Liczba spoza przedzialu.");
    }
    public static void generuj(int tab[], int n, int min, int max) {
        Random generator = new Random();
        int x;
        if (min >= 0) {
            x = max - min + 1;
        }
        else
            x = (-1)*min + max + 1;
        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(x) + min;
        }
    }
    public static void wyswietl(int tab[]){
        System.out.println("Wygenerowane elementy tablicy: ");
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println("\n");
    }
    public static void signum (int tab[]){
        for (int i = 0; i < tab.length; i++) {
            if(tab[i] >= 0) tab[i] = 1;
            else
                tab[i] = -1;
        }
    }
}

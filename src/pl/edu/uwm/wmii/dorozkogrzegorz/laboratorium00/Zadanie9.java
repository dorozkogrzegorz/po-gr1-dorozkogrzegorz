package pl.edu.uwm.wmii.dorozkogrzegorz.laboratorium00;

public class Zadanie9 {
    public static void main(String[] args){
        System.out.println("  /\\_/\\      _ _ _ _");
        System.out.println(" ( o o )    / HELLO  \\");
        System.out.println(" (  ^  )   <  JUNIOR  |");
        System.out.println(" (  =  )    \\ CODER! /");
        System.out.println("  | | |- -|  \\ _____/  ");
        System.out.println("  | | |_ /\\     ");
        System.out.println(" (_ | _)    ");
    }
}

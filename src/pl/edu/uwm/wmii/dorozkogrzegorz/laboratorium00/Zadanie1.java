package pl.edu.uwm.wmii.dorozkogrzegorz.laboratorium00;

public class Zadanie1 {
    int zad1() {
        return 31 + 29 + 31;
    }

    public static void main(String[] args) {
        Zadanie1 suma = new Zadanie1();
        suma.zad1();
        System.out.println("\nSuma dni pierwszych trzech miesięcy dowolnego roku przestępnego wynosi: " + suma.zad1());
    }
}

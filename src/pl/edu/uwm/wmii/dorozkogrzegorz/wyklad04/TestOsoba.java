package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad04;

public class TestOsoba {
    public static void main(String[] args)
    {
        Osoba[] ludzie =new Osoba[2];
        ludzie[0]=new Pracownik1("Kowalski"," Jan Paweł", "1958-09-01",true,5000);
        ludzie[1]=new Student("Nowak"," Małgorzata","1997-02-22",false,"Informatyka",4.6);
        for (Osoba p : ludzie){
            System.out.println(p.getNazwisko()+p.getImiona()+": "+p.płeć()+ p.getOpis());
        }
    }
}
abstract class Osoba
{
    public Osoba(String nazwisko, String imiona, String dataUrodzenia, boolean płeć)
    {
        this.dataUrodzenia=dataUrodzenia;
        this.płeć=płeć;
        this.imiona=imiona;
        this.nazwisko=nazwisko;
    }
    public abstract String getOpis();
    public String getNazwisko()
    {
        return nazwisko;
    }
    private String nazwisko;
    public String getImiona()
    {
        return imiona;
    }
    private String imiona;
    public String dataUrodzenia()
    {
        return dataUrodzenia;
    }
    private String dataUrodzenia;
    public boolean płeć()
    {
        return płeć;
    }
    private boolean płeć;
    public double średniaOcen()
    {
        średniaOcen=4.6;
        return średniaOcen;
    }
    private double średniaOcen;
}
class Pracownik1 extends Osoba
{
    public Pracownik1(String nazwisko, String imiona, String dataUrodzenia, boolean płeć, double pobory)
    {
        super(nazwisko,imiona,dataUrodzenia,płeć);
        this.pobory=pobory;
    }
    public double getPobory()
    {
        return pobory;
    }
    public String getOpis()
    {
        return String.format(" pracownik z pensją %.2f zł",pobory);
    }
    private double pobory;
}
class Student extends Osoba
{
    private double średniaOcen=średniaOcen();

    public Student(String nazwisko, String imiona, String dataUrodzenia, boolean płeć, String kierunek, double średniaOcen)
    {
        super(nazwisko,imiona,dataUrodzenia,płeć);
        this.kierunek=kierunek;
        this.średniaOcen=średniaOcen;
    }
    public String getOpis()
    {
        return " kierunek studiów: "+kierunek+" średnia ocen: "+średniaOcen();
    }
    private String kierunek;
}

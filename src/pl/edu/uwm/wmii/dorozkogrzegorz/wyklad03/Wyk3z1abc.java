package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad03;
import java.util.Scanner;
import java.lang.String;

public class Wyk3z1abc {
    public static void main(String[] args) {
        Scanner xxx = new Scanner(System.in);
        System.out.println("Zad.1a - Wprowadz napis: ");
        String napis=xxx.nextLine();
        System.out.println("Zad.1b - Wprowadz napis: ");
        String napis2=xxx.nextLine();
        System.out.println("Zad.1c - Wprowadz napis: ");
        String napis3=xxx.nextLine();
        char znak ='a';
        countChar(napis,znak);
        countSubStr(napis,napis2);
        middle(napis3);
    }

    public static int countChar(String str,char c){
        int wynik=0;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)==c){
                wynik++;
            }
        }
        System.out.println("Liczba wystapien znaku "+c+" wynosi: "+wynik);
        return wynik;
    }
    public static int countSubStr(String str,String subStr) {
        int wynik=0;
        for(int i=0;i<str.length()-subStr.length()+1;i++) {
            if(subStr.equals(str.substring(i,i+subStr.length()))){
                wynik++;
            }
        }
        System.out.println("Ilosc wystapien ciagu znakow "+subStr+" jest rowna: " + wynik);
        return wynik;
    }
    public static String middle(String str) {
        String wynik="";
        char wynik2='a';
        if(str.length()/2%2==1) {
            wynik = str.substring(str.length() / 2 - 1, str.length() / 2 + 1);
            System.out.println(wynik);
        }
        else
            wynik2=str.charAt(str.length()/2);
        System.out.println("Srodkowy znak napisu: "+wynik2);
        return wynik;
    }
}
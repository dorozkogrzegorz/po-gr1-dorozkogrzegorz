package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad03;

public class Wyk3z1f {
    public static void main(String[] args){
        StringBuffer str = new StringBuffer("AnTyBiOtYkOtErApIa");
        Change(str);
        System.out.println("Efekt zmiany napisu: " + str);
    }
    public static void Change(StringBuffer str) {
        for (int i = 0; i < str.length(); i++) {
            Character x = str.charAt(i);
            if (Character.isUpperCase(x))
                str.replace(i, i + 1, Character.toLowerCase(x) + "");
            else
                str.replace(i, i + 1, Character.toUpperCase(x) + "");

        }
    }
}

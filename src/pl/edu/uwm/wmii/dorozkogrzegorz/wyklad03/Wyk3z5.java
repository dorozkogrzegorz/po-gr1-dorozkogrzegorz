package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad03;
import java.math.BigDecimal;
import java.util.Scanner;
import java.lang.String;

public class Wyk3z5 {
    public static void main(String[] args){
        System.out.println("Wprowadź kapitał początkowy: ");
        Scanner k = new Scanner(System.in);
        BigDecimal kwota=k.nextBigDecimal();
        System.out.println("Wprowadź stopę oprocentowania: ");
        BigDecimal stopa=k.nextBigDecimal();
        System.out.println("Wprowadź okres czasu (w latach): ");
        int n =k.nextInt();
        BigDecimal p= new BigDecimal(100);
        BigDecimal dzielenie= stopa.divide(p);
        BigDecimal raz=new BigDecimal(1);
        BigDecimal kapital=kwota.multiply((raz.add(dzielenie)).pow(n));
        System.out.println("Kapitał po "+n+" latach wynosi "+kapital);

    }
}


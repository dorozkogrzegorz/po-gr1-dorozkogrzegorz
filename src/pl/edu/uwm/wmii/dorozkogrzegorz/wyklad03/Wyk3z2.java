package pl.edu.uwm.wmii.dorozkogrzegorz.wyklad03;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;
import java.lang.String;
import java.io.IOException;

public class Wyk3z2 {
    public static void main (String[] args) throws IOException{
        BufferedReader odczyt = new BufferedReader(new FileReader("C:\\Users\\Grzesiek\\Desktop\\brudnopis.txt"));
        int a;
        System.out.println("Podaj literę: ");
        Scanner tekst = new Scanner(System.in);

        char c = tekst.next().charAt(0);
        int ileliter = 0;
        while ((a = odczyt.read()) != -1) {
            if (c == (char) a) {
                ileliter++;
            }
        }
        odczyt.close();

        System.out.println("Litera " + c + " występuje " + ileliter + " razy.");
    }
}
